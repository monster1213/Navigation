//在线列表组件
Vue.component('online-list', {
    template: '<ul><li v-for="(uid, nick) in list" data-uid="{{uid}}">{{nick}}</li></ul>',
    data: function() {
        return {
        };
    },
    props: {
        list: {
            required: true,
            type: Object
        },
        onOpen: {
            required: false,
            type: Function
        }
    },
    watch: {
        list: function(v) {
            console.log(v);
        }
    },
    ready: function() {
        var self = this;

        $(this.$el).on("click", "li", function() {
            var uid = $(this).data("uid");
            var nick = getNick(self.list[uid]);
            if (self.onOpen) {
                self.onOpen(uid, nick);
            }
        });
    }
});

//近期聊天列表组件
Vue.component('recent-list', {
    template: '<ul><li v-for="item in list" :class="{\'active\':item.key==current}" v-on:click="switchTo($index)" track-by="key">'
        + '<div class="recent-nick">{{item.name}}</div><div class="recent-msg">{{item.text}}</div></li></ul>',
    data: function() {
        return {
        };
    },
    props: {
        list: {
            required: true,
            type: Array
        },
        current: {
            type: String,
            default: null
        },
        onSwitch: {
            required: false,
            type: Function
        }
    },
    methods: {
        switchTo: function(i) {
            var item = this.list[i];

            if (item.key != this.current) {
                this.current = item.key;
                if (this.onSwitch) {
                    this.onSwitch(item.key, item.name);
                }
            }
        }
    },
    ready: function() {
        var self = this;

    }
});

//消息列表组件
Vue.component('message-box', {
    template: '<ul class="chat-pop"><component v-for="item in messages" :is="item.tpl" :item="item"></component></ul>',
    props: {
        messages: []
    },
    ready: function() {

    },
    components: {
        //普通文本消息
        'message-item': {
            template: '<li class="[\'message\', item.type ? \'message-send\' : \'message-receive\']" id="{{item.id}}">'
                + '<div class="message-head">'
                + '<span class="message-nick">{{item.nick}}</span><span class="message-time">{{item.time}}</span>'
                + '</div><div class="message-content">{{item.content}}</div></li>',
            props: {
                item: {
                    required: true,
                    type: Object
                }
            }
        },
        //小提示
        'tip': {
            template: '<li class="tip"><span>{{item.content}}</span></li>',
            props: {
                item: {
                    required: true,
                    type: Object
                }
            }
        }
    }
});